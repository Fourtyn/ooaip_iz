﻿namespace OOAiP_IZ
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.labelInfo = new System.Windows.Forms.Label();
            this.labelInfoHeader = new System.Windows.Forms.Label();
            this.StudentsBigPanel = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.listBoxStudents = new System.Windows.Forms.ListBox();
            this.comboBoxGroups = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.OrdersBigPanel = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.listBoxOrders = new System.Windows.Forms.ListBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.buttonCreateOrder = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.listBoxNewOrders = new System.Windows.Forms.ListBox();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.StudentsBigPanel.SuspendLayout();
            this.OrdersBigPanel.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.labelInfoHeader);
            this.panel1.Location = new System.Drawing.Point(836, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(416, 657);
            this.panel1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.AutoScroll = true;
            this.panel3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel3.Controls.Add(this.labelInfo);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 76);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(416, 581);
            this.panel3.TabIndex = 4;
            // 
            // labelInfo
            // 
            this.labelInfo.AllowDrop = true;
            this.labelInfo.AutoSize = true;
            this.labelInfo.BackColor = System.Drawing.SystemColors.Control;
            this.labelInfo.Font = new System.Drawing.Font("Calibri", 14F);
            this.labelInfo.Location = new System.Drawing.Point(3, 0);
            this.labelInfo.MaximumSize = new System.Drawing.Size(410, 0);
            this.labelInfo.MinimumSize = new System.Drawing.Size(390, 615);
            this.labelInfo.Name = "labelInfo";
            this.labelInfo.Size = new System.Drawing.Size(390, 615);
            this.labelInfo.TabIndex = 0;
            this.labelInfo.Text = "Здесь будет информация";
            // 
            // labelInfoHeader
            // 
            this.labelInfoHeader.BackColor = System.Drawing.SystemColors.ControlLight;
            this.labelInfoHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelInfoHeader.Font = new System.Drawing.Font("Calibri", 20F, System.Drawing.FontStyle.Italic);
            this.labelInfoHeader.Location = new System.Drawing.Point(0, 0);
            this.labelInfoHeader.Name = "labelInfoHeader";
            this.labelInfoHeader.Size = new System.Drawing.Size(416, 76);
            this.labelInfoHeader.TabIndex = 3;
            this.labelInfoHeader.Text = "Просмотр информации:";
            this.labelInfoHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // StudentsBigPanel
            // 
            this.StudentsBigPanel.BackColor = System.Drawing.SystemColors.ControlLight;
            this.StudentsBigPanel.Controls.Add(this.label4);
            this.StudentsBigPanel.Controls.Add(this.listBoxStudents);
            this.StudentsBigPanel.Controls.Add(this.comboBoxGroups);
            this.StudentsBigPanel.Controls.Add(this.label3);
            this.StudentsBigPanel.Location = new System.Drawing.Point(15, 12);
            this.StudentsBigPanel.Name = "StudentsBigPanel";
            this.StudentsBigPanel.Size = new System.Drawing.Size(564, 240);
            this.StudentsBigPanel.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AllowDrop = true;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Italic);
            this.label4.Location = new System.Drawing.Point(202, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(324, 23);
            this.label4.TabIndex = 3;
            this.label4.Text = "Выберите студента для просмотра:";
            // 
            // listBoxStudents
            // 
            this.listBoxStudents.Font = new System.Drawing.Font("Calibri", 14.25F);
            this.listBoxStudents.FormattingEnabled = true;
            this.listBoxStudents.ItemHeight = 23;
            this.listBoxStudents.Location = new System.Drawing.Point(192, 52);
            this.listBoxStudents.Name = "listBoxStudents";
            this.listBoxStudents.Size = new System.Drawing.Size(345, 165);
            this.listBoxStudents.TabIndex = 2;
            this.listBoxStudents.SelectedIndexChanged += new System.EventHandler(this.listBoxStudents_SelectedIndexChanged);
            // 
            // comboBoxGroups
            // 
            this.comboBoxGroups.FormattingEnabled = true;
            this.comboBoxGroups.Items.AddRange(new object[] {
            "Все",
            "КТбо1-1",
            "КТбо2-2",
            "КТбо2-3",
            "КТбо3-4",
            "КТбо4-5"});
            this.comboBoxGroups.Location = new System.Drawing.Point(8, 80);
            this.comboBoxGroups.Name = "comboBoxGroups";
            this.comboBoxGroups.Size = new System.Drawing.Size(152, 21);
            this.comboBoxGroups.TabIndex = 1;
            this.comboBoxGroups.SelectedIndexChanged += new System.EventHandler(this.comboBoxGroups_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AllowDrop = true;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Italic);
            this.label3.Location = new System.Drawing.Point(9, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(156, 46);
            this.label3.TabIndex = 0;
            this.label3.Text = "Выберите группу\r\nдля просмотра:";
            // 
            // OrdersBigPanel
            // 
            this.OrdersBigPanel.BackColor = System.Drawing.SystemColors.ControlLight;
            this.OrdersBigPanel.Controls.Add(this.label5);
            this.OrdersBigPanel.Controls.Add(this.listBoxOrders);
            this.OrdersBigPanel.Location = new System.Drawing.Point(442, 273);
            this.OrdersBigPanel.Name = "OrdersBigPanel";
            this.OrdersBigPanel.Size = new System.Drawing.Size(358, 363);
            this.OrdersBigPanel.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AllowDrop = true;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Italic);
            this.label5.Location = new System.Drawing.Point(28, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(295, 23);
            this.label5.TabIndex = 4;
            this.label5.Text = "Выберите приказ для просмотра:";
            // 
            // listBoxOrders
            // 
            this.listBoxOrders.Font = new System.Drawing.Font("Calibri", 14.25F);
            this.listBoxOrders.FormattingEnabled = true;
            this.listBoxOrders.ItemHeight = 23;
            this.listBoxOrders.Location = new System.Drawing.Point(7, 52);
            this.listBoxOrders.Name = "listBoxOrders";
            this.listBoxOrders.Size = new System.Drawing.Size(342, 303);
            this.listBoxOrders.TabIndex = 3;
            this.listBoxOrders.SelectedIndexChanged += new System.EventHandler(this.listBoxOrders_SelectedIndexChanged);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel2.Controls.Add(this.buttonCreateOrder);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.listBoxNewOrders);
            this.panel2.Location = new System.Drawing.Point(15, 273);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(358, 363);
            this.panel2.TabIndex = 5;
            // 
            // buttonCreateOrder
            // 
            this.buttonCreateOrder.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.buttonCreateOrder.Font = new System.Drawing.Font("Calibri", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.buttonCreateOrder.Location = new System.Drawing.Point(98, 240);
            this.buttonCreateOrder.Name = "buttonCreateOrder";
            this.buttonCreateOrder.Size = new System.Drawing.Size(156, 89);
            this.buttonCreateOrder.TabIndex = 5;
            this.buttonCreateOrder.Text = "Создать приказ";
            this.buttonCreateOrder.UseVisualStyleBackColor = false;
            this.buttonCreateOrder.Click += new System.EventHandler(this.buttonCreateOrder_Click);
            // 
            // label6
            // 
            this.label6.AllowDrop = true;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Italic);
            this.label6.Location = new System.Drawing.Point(9, 17);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(330, 23);
            this.label6.TabIndex = 4;
            this.label6.Text = "Выберите тип создаваемого приказа:";
            // 
            // listBoxNewOrders
            // 
            this.listBoxNewOrders.Font = new System.Drawing.Font("Calibri", 14.25F);
            this.listBoxNewOrders.FormattingEnabled = true;
            this.listBoxNewOrders.ItemHeight = 23;
            this.listBoxNewOrders.Items.AddRange(new object[] {
            "Приказ на начисление стипендии",
            "Приказ на отчисление",
            "Приказ на присвоение звания бакалавра",
            "Приказ на перевод на следующий курс"});
            this.listBoxNewOrders.Location = new System.Drawing.Point(7, 52);
            this.listBoxNewOrders.Name = "listBoxNewOrders";
            this.listBoxNewOrders.Size = new System.Drawing.Size(342, 165);
            this.listBoxNewOrders.TabIndex = 3;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::OOAiP_IZ.Properties.Resources.TRTU_G_2;
            this.ClientSize = new System.Drawing.Size(1264, 681);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.OrdersBigPanel);
            this.Controls.Add(this.StudentsBigPanel);
            this.Controls.Add(this.panel1);
            this.Name = "Form2";
            this.Text = "Form2";
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.StudentsBigPanel.ResumeLayout(false);
            this.StudentsBigPanel.PerformLayout();
            this.OrdersBigPanel.ResumeLayout(false);
            this.OrdersBigPanel.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelInfoHeader;
        private System.Windows.Forms.Panel StudentsBigPanel;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label labelInfo;
        private System.Windows.Forms.ListBox listBoxStudents;
        private System.Windows.Forms.ComboBox comboBoxGroups;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel OrdersBigPanel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ListBox listBoxOrders;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ListBox listBoxNewOrders;
        private System.Windows.Forms.Button buttonCreateOrder;
    }
}
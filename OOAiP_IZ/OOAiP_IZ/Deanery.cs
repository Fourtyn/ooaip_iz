﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOAiP_IZ
{
    public struct QualWorksRecord
    {
        public int theme;
        public string Teacher;
        public Student author;
    }
    public abstract class DeaneryOrder
    {
        public int id;

        public int type;

        public string text = "";
    }

    public class StudentsOrder : DeaneryOrder
    {
        public List<Student> students;
        public StudentsOrder(int orderId, int orderType, List<Student> studentsAffected, string orderText)
        {
            id = orderId;
            type = orderType;
            students = studentsAffected;
            text = orderText;
        }
    }

    public class QualWorksOrder : DeaneryOrder
    {
        public List<QualWorksRecord> table;
        public QualWorksOrder(int orderId, int orderType, List<QualWorksRecord> correspondence, string orderText)
        {
            id = orderId;
            type = orderType;
            table = correspondence;
            text = orderText;
        }
    }
    public class OrderQualWorksMaker
    {
        public List<Student> FilterStudents()
        {
            University un = University.Instance;

            List<Student> list = new List<Student>();

            foreach (Student s in un.Students)
            {
                if (s.course == 4)
                {
                    list.Add(s);
                }
            }

            return list;
        }
        public QualWorksOrder MakeOrderQualWorks(int id)
        {
            StringBuilder sb = new StringBuilder();

            // "Приказ №1\n"
            sb.AppendLine(strFormOrder.orderWithNum + id.ToString());

            sb.AppendLine("Приказываю назначить студентам тему квалификационной работы и научного руководителя:");

            List<Student> students = FilterStudents();
            List<QualWorksRecord> qwTable = new List<QualWorksRecord>();
            // Вывод студентов
            if (students.Count > 0)
            {
                foreach (Student s in students)
                {
                    sb.AppendLine(strFormOrder.studentGr + s.group.Name + ' ' + s.name + " - " + strFormOrder.themeNum + s.work.theme + " - " + s.work.Teacher);

                    QualWorksRecord qwRecord = new QualWorksRecord()
                    { 
                        author = s, 
                        theme = s.work.theme, 
                        Teacher = s.work.Teacher 
                    };
                    qwTable.Add(qwRecord);
                }
            }
            else
            {
                sb.AppendLine("Нет студентов 4 курса!");
            }

            return new QualWorksOrder(id, (int)Deanery.OrderTypes.QualWorks, qwTable, sb.ToString());
        }
    }
    public class OrderCourseUpMaker
    {
        public List<Student> FilterStudents()
        {
            University un = University.Instance;

            List<Student> list = new List<Student>();

            foreach(Student s in un.Students)
            {
                if(s.course < 4 && !Deanery.hasMarksTwo(s))
                {
                    list.Add(s);
                }
            }

            return list;
        }
        public StudentsOrder MakeOrderCourseUp(int id)
        {
            StringBuilder sb = new StringBuilder();

            // "Приказ №1\n"
            sb.AppendLine(strFormOrder.orderWithNum + id.ToString()); 

            // Приказываю перевести на следующий курс студентов ниже: 
            sb.AppendLine(strFormOrder.iOrderTo + strFormOrder.toCourseUp + strFormOrder.studentov); 

            List<Student> students = FilterStudents();
            // Вывод студентов
            if(students.Count > 0)
            {
                foreach(Student s in students)
                {
                    sb.AppendLine(strFormOrder.studentGr + s.group.Name + ' ' + s.name);
                }
            }
            else
            {
                sb.AppendLine("Ни один студент не переведен!");
            }

            return new StudentsOrder(id, (int)Deanery.OrderTypes.CourseUp, students, sb.ToString());
        }
    }
    public class OrderDismissMaker
    {
        public List<Student> FilterStudents()
        {
            University un = University.Instance;

            List<Student> list = new List<Student>();

            foreach (Student s in un.Students)
            {
                if (Deanery.hasMarksTwo(s))
                {
                    list.Add(s);
                }
                else
                {
                    if(s.course == 4 && s.work != null)
                        if(s.work.mark == 2)
                            list.Add(s);
                }
            }

            return list;
        }
        public StudentsOrder MakeOrderDismiss(int id)
        {
            StringBuilder sb = new StringBuilder();
            // "Приказ №1\n"
            sb.AppendLine(strFormOrder.orderWithNum + id.ToString());
            // Приказываю отчислить студентов ниже: 
            sb.AppendLine(strFormOrder.iOrderTo + strFormOrder.toDismiss + strFormOrder.studentov);

            List<Student> students = FilterStudents();
            // Вывод студентов
            if (students.Count > 0)
            {
                foreach (Student s in students)
                {
                    sb.AppendLine(strFormOrder.studentGr + s.group.Name + ' ' + s.name);
                }
            }
            else
            {
                sb.AppendLine("Ни один студент не отчислен!");
            }

            return new StudentsOrder(id, (int)Deanery.OrderTypes.Dismiss, students, sb.ToString());
        }
    }
    public class OrderGiveStipendMaker
    {
        public List<Student> FilterStudents()
        {
            University un = University.Instance;

            List<Student> list = new List<Student>();

            foreach (Student s in un.Students)
            {
                if (s.course != 4 && !Deanery.hasNegativeMarks(s))
                {
                    list.Add(s);
                }
            }

            return list;
        }
        public StudentsOrder MakeOrderGiveStipend(int id)
        {
            StringBuilder sb = new StringBuilder();
            // "Приказ №1\n"
            sb.AppendLine(strFormOrder.orderWithNum + id.ToString());
            // Приказываю выплачивать академические стипендии студентам ниже: 
            sb.AppendLine(strFormOrder.iOrderTo + strFormOrder.toGiveStipend + strFormOrder.studentam);

            List<Student> students = FilterStudents();
            // Вывод студентов
            if (students.Count > 0)
            {
                foreach (Student s in students)
                {
                    sb.AppendLine(strFormOrder.studentGr + s.group.Name + ' ' + s.name);
                }
            }
            else
            {
                sb.AppendLine("Ни одному студенту не будут выплачиваться стипендии!");
            }

            return new StudentsOrder(id, (int)Deanery.OrderTypes.GiveStipend, students, sb.ToString());
        }
    }
    public class OrderGraduateMaker
    {
        public List<Student> FilterStudents()
        {
            University un = University.Instance;

            List<Student> list = new List<Student>();

            foreach (Student s in un.Students)
            {
                if (s.course == 4 && s.work != null && !Deanery.hasMarksTwo(s))
                {
                    if(s.work.mark != 2)
                        list.Add(s);
                }
            }

            return list;
        }
        public StudentsOrder MakeOrderGraduate(int id)
        {
            StringBuilder sb = new StringBuilder();
            // "Приказ №1\n"
            sb.AppendLine(strFormOrder.orderWithNum + id.ToString());
            // Приказываю присвоить звание бакалавра студентам ниже: 
            sb.AppendLine(strFormOrder.iOrderTo + strFormOrder.toGraduate + strFormOrder.studentam);

            List<Student> students = FilterStudents();
            // Вывод студентов
            if (students.Count > 0)
            {
                foreach (Student s in students)
                {
                    sb.AppendLine(strFormOrder.studentGr + s.group.Name + ' ' + s.name);
                }
            }
            else
            {
                sb.AppendLine("Ни одному студенту не будет присвоено звание бакалавра!");
            }

            return new StudentsOrder(id, (int)Deanery.OrderTypes.Graduate, students, sb.ToString());
        }
    }

    // Деканат
    public class Deanery
    {
        // Одиночка
        public static readonly Deanery _inst = new Deanery(new OrderCourseUpMaker(), 
            new OrderDismissMaker(), new OrderGiveStipendMaker(), 
            new OrderGraduateMaker(), new OrderQualWorksMaker());

        public static Deanery Instance
        {
            get { return _inst; }
        }

        // Фасад
        OrderCourseUpMaker courseUp;
        OrderDismissMaker dismiss;
        OrderGiveStipendMaker giveStipend;
        OrderGraduateMaker graduate;
        OrderQualWorksMaker qualWorkInfo;

        public Deanery(OrderCourseUpMaker toCourseUpOrder,
            OrderDismissMaker toDismiss,
            OrderGiveStipendMaker toStipend,
            OrderGraduateMaker toGraduate,
            OrderQualWorksMaker qualWorkMaker)
        {
            courseUp = toCourseUpOrder;
            dismiss = toDismiss;
            giveStipend = toStipend;
            graduate = toGraduate;
            qualWorkInfo = qualWorkMaker;
        }

        public void MakeOrder(int orderType)
        {
            DeaneryOrder order;
            switch(orderType)
            {
                case (int)OrderTypes.CourseUp:
                    {
                        order = courseUp.MakeOrderCourseUp(++orderCounter);
                        University.Instance.Orders.Add(order);
                        break;
                    }
                case (int)OrderTypes.Dismiss:
                    {
                        order = dismiss.MakeOrderDismiss(++orderCounter);
                        University.Instance.Orders.Add(order);
                        break;
                    }
                case (int)OrderTypes.GiveStipend:
                    {
                        order = giveStipend.MakeOrderGiveStipend(++orderCounter);
                        University.Instance.Orders.Add(order);
                        break;
                    }
                case (int)OrderTypes.Graduate:
                    {
                        order = graduate.MakeOrderGraduate(++orderCounter);
                        University.Instance.Orders.Add(order);
                        break;
                    }
                case (int)OrderTypes.QualWorks:
                    {
                        order = qualWorkInfo.MakeOrderQualWorks(++orderCounter);
                        University.Instance.Orders.Add(order);
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }

        // Деканат

        public int orderCounter = 0;

        public enum OrderTypes:int
        {
            QualWorks = 1,
            CourseUp = 2,
            Dismiss = 3,
            GiveStipend = 4,
            Graduate = 5
        }

        public static bool hasMarksTwo(Student st)
        {
            bool res = false;
            foreach (int m in st.marks)
            {
                if (m == 2)
                {
                    res = true;
                    break;
                }
            }

            return res;
        }

        public static bool hasNegativeMarks(Student st)
        {
            bool res = false;
            foreach (int m in st.marks)
            {
                if (m < 4)
                {
                    res = true;
                    break;
                }
            }

            return res;
        }

        public void ExecuteOrder(int id)
        {
            // Находим нужный приказ
            DeaneryOrder baseOrder = University.Instance.Orders.Find(x => x.id == id);
            if(baseOrder.type != (int)OrderTypes.QualWorks)
            {
                StudentsOrder order = (StudentsOrder)baseOrder;
                switch (order.type)
                {
                    case (int)OrderTypes.CourseUp:
                        {
                            SetToCourseUp(order.students);
                            break;
                        }
                    case (int)OrderTypes.Dismiss:
                        {
                            SetDismissing(order.students);
                            break;
                        }
                    case (int)OrderTypes.GiveStipend:
                        {
                            SetStipend(order.students);
                            break;
                        }
                    case (int)OrderTypes.Graduate:
                        {
                            SetGraduating(order.students);
                            break;
                        }
                }
            }
        }

        // Заполнение студентам полей по приказам
        public void SetStipend(List<Student> studentsToStipend)
        {
            foreach (Student s in University.Instance.Students)
            {
                if(s.course < 4)
                {
                    if (studentsToStipend.Contains(s))
                        s.isGettingStipend = true;
                    else
                        s.isGettingStipend = false;
                }
            }
        }
        public void SetToCourseUp(List<Student> students)
        {
            foreach (Student s in students)
            {
                s.isToCourseUp = true;
            }
        }
        public void SetDismissing(List<Student> students)
        {
            foreach (Student s in students)
            {
                s.isToDismiss = true;
            }
        }
        public void SetGraduating(List<Student> students)
        {
            foreach (Student s in students)
            {
                s.isToGraduate = true;
            }
        }
    }

    // Строки
    public static class strFormOrder
    {
        public static string orderWithNum = "Приказ №";
        public static string iOrderTo = "Приказываю ";
        public static string toDismiss = "отчислить ";
        public static string toGraduate = "присвоить звание бакалавра ";
        public static string toCourseUp = "перевести на следующий курс ";
        public static string toGiveStipend = "выплачивать академические стипендии ";
        public static string studentov = "студентов ниже: ";
        public static string studentam = "студентам ниже: ";
        public static string studentGr = "   - студент гр. ";
        public static string themeNum = "Тема №";
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOAiP_IZ
{
    public partial class Form2 : Form
    {
        private string viewInfo = "Просмотр информации о ";
        private string viewInfoStudent = "студенте:";
        private string viewInfoOrder = "приказе:";

        private string stipendOrderWord = "Приказ на начисление стипендии";
        private string dismissOrderWord = "Приказ на отчисление";
        private string graduationOrderWord = "Приказ на присвоение звания бакалавра";
        private string courseUpOrderWord = "Приказ на перевод на следующий курс";

        private void InitFormParams()
        {
            this.StartPosition = FormStartPosition.CenterParent;
            this.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            this.Width = 1280;
            this.Height = 720;
            this.MaximizeBox = false;
        }
        private void InitFormValues()
        {
            // Заполняем listbox новых приказов
            string[] orderTypeNames = { stipendOrderWord , dismissOrderWord , graduationOrderWord , courseUpOrderWord };
            listBoxNewOrders.DataSource = orderTypeNames;
        }

        private void AddOrderToListBox(DeaneryOrder order)
        {
            listBoxOrders.Items.Add(order.id.ToString());
        }

        private void CreateQualWorkOrder()
        {
            Deanery.Instance.MakeOrder((int)Deanery.OrderTypes.QualWorks);
            AddOrderToListBox(University.Instance.Orders.Last());
        }

        private void comboBoxGroups_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedState = comboBoxGroups.SelectedItem.ToString();
            //MessageBox.Show(selectedState);

            if(selectedState == "Все")
            {
                ShowStudentsInListBox();
            }
            else
            {
                Group gr = University.Instance.Groups.Find(x => x.Name == selectedState);
                ShowStudentsInListBox(gr);
            }
        }

        private void ShowStudentsInListBox(Group gr)
        {
            listBoxStudents.DisplayMember = "name";
            listBoxStudents.ValueMember = "id";
            listBoxStudents.DataSource = gr.students;
        }

        private void ShowStudentsInListBox()
        {
            listBoxStudents.DisplayMember = "name";
            listBoxStudents.ValueMember = "id";
            listBoxStudents.DataSource = University.Instance.Students;
        }

        private void ShowOrdersInListBox()
        {
            // Заполняем listbox уже созданных приказов
            listBoxOrders.DataSource = University.Instance.Orders;
            listBoxOrders.DisplayMember = "type";
            listBoxOrders.ValueMember = "id";
        }

        private void listBoxStudents_SelectedIndexChanged(object sender, EventArgs e)
        {
            Student stud = University.Instance.Students.Find(x => x.id == (int)listBoxStudents.SelectedValue);

            if (stud != null)
            {
                labelInfoHeader.Text = viewInfo + viewInfoStudent;
                labelInfo.Text = stud.GetInfo();
            }
            else
                MessageBox.Show("Stud null");
        }

        private void ShowOrderInInfoLabel(DeaneryOrder order)
        {
            labelInfoHeader.Text = viewInfo + viewInfoOrder;
            labelInfo.Text = order.text;
        }

        public Form2()
        {
            InitFormParams();

            InitializeComponent();

            InitFormValues();

            CreateQualWorkOrder();
        }

        private void vScrollBarInfoLabel_Scroll(object sender, ScrollEventArgs e)
        {
            
        }

        private void listBoxOrders_SelectedIndexChanged(object sender, EventArgs e)
        {
            // ListBox Index начинается с "0", номера приказов - с "1".
            DeaneryOrder order = University.Instance.Orders.Find(x => x.id == listBoxOrders.SelectedIndex + 1);

            if (order != null)
            {
                ShowOrderInInfoLabel(order);
            }
            else
                MessageBox.Show("order null");
        }

        private void buttonCreateOrder_Click(object sender, EventArgs e)
        {
            if(listBoxNewOrders.SelectedItem != null)
            {
                int orderType = -1;
                //MessageBox.Show(listBoxNewOrders.SelectedItem.ToString());

                if (listBoxNewOrders.SelectedItem.ToString() == stipendOrderWord)
                    orderType = (int)Deanery.OrderTypes.GiveStipend;
                else
                {
                    if (listBoxNewOrders.SelectedItem.ToString() == dismissOrderWord)
                        orderType = (int)Deanery.OrderTypes.Dismiss;
                    else
                    {
                        if (listBoxNewOrders.SelectedItem.ToString() == graduationOrderWord)
                            orderType = (int)Deanery.OrderTypes.Graduate;
                        else
                        {
                            orderType = (int)Deanery.OrderTypes.CourseUp;
                        }
                    }
                }

                if(orderType != -1)
                {
                    // Создаем приказ
                    switch(orderType)
                    {
                        // передаем в MakeOrder() тип приказа - сопоставляем с enum OrderTypes
                        
                        // выдача стипендии
                        case (int)Deanery.OrderTypes.GiveStipend:
                            Deanery.Instance.MakeOrder((int)Deanery.OrderTypes.GiveStipend);
                            break;

                        // повышение курса
                        case (int)Deanery.OrderTypes.CourseUp:
                            Deanery.Instance.MakeOrder((int)Deanery.OrderTypes.CourseUp);
                            break;

                        // отчисление
                        case (int)Deanery.OrderTypes.Dismiss:
                            Deanery.Instance.MakeOrder((int)Deanery.OrderTypes.Dismiss);
                            break;

                        // выпуск (присвоение звания бакалавра)
                        case (int)Deanery.OrderTypes.Graduate:
                            Deanery.Instance.MakeOrder((int)Deanery.OrderTypes.Graduate);
                            break;
                    }

                    //// Заполняем listbox уже созданных приказов
                    //ShowOrdersInListBox();

                    // Получаем созданный приказ
                    DeaneryOrder newOrder = University.Instance.Orders.Last();

                    // Исполнение приказа
                    Deanery.Instance.ExecuteOrder(newOrder.id);

                    // Вывод приказа
                    ShowOrderInInfoLabel(newOrder);

                    // Заносим приказ в ListBox
                    AddOrderToListBox(newOrder);
                }
                else
                {
                    MessageBox.Show("Ошибка! Тип приказа не выставлен!");
                }
            }
            else
                MessageBox.Show("Выберите тип приказа!");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOAiP_IZ
{
    public class Generator
    {
        public static readonly Generator _inst = new Generator();
        public static Generator Instance
        {
            get { return _inst; }
        }

        public Random rnd = new Random();

        string[] surNames = new[] { "Иванов", "Смирнов", "Кузнецов", "Попов", "Васильев", "Петров",
            "Соколов", "Михайлов", "Новиков", "Федоров", "Сорокин", "Мозговой", "Романов", "Семенихин",
            "Гордиенко", "Лобачевский", "ДиКаприо", "Питт", "Палпатин", "Эйнштейн", "Литвиненко", "Кеноби",
            "Сковородкин", "Романенко", "Ривский", "Ким", "Аршинов", "Арысланов", "Асатов", "Зубенко", 
            "Бендер", "Паниковский", "Лебовски", "Броден", "Сундстрем", "Гейтс", "Кнут", "Соболев", 
            "Музыченко", "Максимов", "Ильф", "Воробьянинов", "Балаганов", "Козлевич", "Корейко", "Шмидт"};

        string[] firstNames = new[] { "Александр", "Максим", "Даниил", "Дмитрий", "Иван", "Кирилл",
            "Никита", "Михаил", "Егор", "Матвей", "Андрей", "Илья", "Алексей", "Роман", "Сергей",
            "Владислав", "Ярослав", "Тимофей", "Арсений", "Денис", "Владимир", "Павел", "Глеб",
            "Константин", "Захар", "Акакий", "Остап" };

        string[] midNames = new[] { "Александрович", "Алексеевич", "Анатольевич", "Андреевич",
            "Антонович", "Аркадьевич", "Артемович", "Бедросович", "Богданович", "Борисович",
            "Валентинович", "Валерьевич", "Васильевич", "Викторович", "Витальевич", "Владимирович",
            "Владиславович", "Вольфович", "Вячеславович", "Геннадиевич", "Георгиевич", "Григорьевич", "Ассанович" };

        string[] femSurnames = new[] { "Иванова", "Смирнова", "Кузнецова", "Попова", "Васильева", "Петрова", "Соколова",
            "Михайлова", "Новикова", "Федорова", "Сорокина", "Романова", "Семенихина", "Гордиенко", "Лобачевская",
            "Джоли", "Литвиненко", "Сковородкина", "Романенко", "Ривская", "Аршинова", "Арысланова", "Асатова", "Зубенко",
            "Соболева", "Максимова", "Воробьянинова", "Балаганова", "Корейко", "Шмидт" };

        string[] femFirstNames = new[] { "Александра", "Алина", "Алиса", "Анастасия", "Анна", "Вероника", "Виктория",
            "Дарья", "Екатерина", "Елена", "Жанна", "Кристина", "Ксения", "Марина", "Мария", "Наталья", "Ольга",
            "Полина", "Рената", "Светлана", "Снежана", "София", "Эльвира" };

        string[] femMidNames = new[] { "Александровна", "Анатольевна", "Андреевна", "Антоновна", 
            "Борисовна", "Валентиновна", "Валерьевна", "Васильевна", "Геннадиевна", "Георгиевна",
            "Григорьевна", "Даниловна", "Евгеньевна", "Егоровна", "Константиновна", "Леонидовна",
            "Львовна", "Максимовна", "Матвеевна", "Михайловна", "Николаевна", "Олеговна",
            "Павловна", "Петровна", "Романовна", "Семеновна", "Степановна",
            "Тимофеевна", "Федоровна", "Филипповна", "Юрьевна", "Яковлевна" };
        
        List<int[]> nameCombinations = new List<int[]>();
        List<int[]> femNameCombinations = new List<int[]>();

        public string GenerateName()
        {
            if (nameCombinations.Count == surNames.Length * firstNames.Length * midNames.Length)
                return null; // Все имена заняты

            string res = null;
            bool foundNewName = false;
            while(!foundNewName)
            {
                int indexSurname, indexFirstname, indexMidname;
                int[] currentComb;

                bool sexIsMale;
                if (rnd.Next() % 2 == 0)
                    sexIsMale = true;
                else
                    sexIsMale = false;

                if (sexIsMale)
                {
                    // Студент мужского пола
                    indexSurname = rnd.Next() % surNames.Length;
                    indexFirstname = rnd.Next() % firstNames.Length;
                    indexMidname = rnd.Next() % midNames.Length;

                    foundNewName = true;
                    currentComb = new[] { indexSurname, indexFirstname, indexMidname };
                    foreach (int[] l in nameCombinations)
                    {
                        if (l.Equals(currentComb))
                        {
                            foundNewName = false;
                            break;
                        }
                    }
                }
                else
                {
                    // Студент женского пола
                    indexSurname = rnd.Next() % femSurnames.Length;
                    indexFirstname = rnd.Next() % femFirstNames.Length;
                    indexMidname = rnd.Next() % femMidNames.Length;

                    foundNewName = true;
                    currentComb = new[] { indexSurname, indexFirstname, indexMidname };
                    foreach (int[] l in femNameCombinations)
                    {
                        if (l.Equals(currentComb))
                        {
                            foundNewName = false;
                            break;
                        }
                    }
                }

                
                if(foundNewName)
                {
                    if(sexIsMale)
                    {
                        // Студент мужского пола
                        res = surNames[indexSurname] + " " + firstNames[indexFirstname] + " " + midNames[indexMidname];
                        nameCombinations.Add(currentComb);
                    }
                    else
                    {
                        // Студент женского пола
                        res = femSurnames[indexSurname] + " " + femFirstNames[indexFirstname] + " " + femMidNames[indexMidname];
                        femNameCombinations.Add(currentComb);
                    }
                }
            }

            return res;
        }

        public Group GenerateGroup(int course)
        {
            switch (course)
            {
                case 1:
                    {
                        return University.Instance.Groups.Find(x => x.Name == "КТбо1-1");
                    }
                case 2:
                    {
                        if (rnd.Next() % 2 == 0)
                            return University.Instance.Groups.Find(x => x.Name == "КТбо2-2");
                        else
                            return University.Instance.Groups.Find(x => x.Name == "КТбо2-3");
                    }
                case 3:
                    {
                        return University.Instance.Groups.Find(x => x.Name == "КТбо3-4");
                    }
                case 4:
                    {
                        return University.Instance.Groups.Find(x => x.Name == "КТбо4-5");
                    }
                default:
                    {
                        return null;
                    }
            }
        }

        // Returns random teacher
        public string GenerateTeacher()
        {
            return University.Instance.Teachers[rnd.Next() % University.Instance.Teachers.Length];
        }

        // Returns random marks
        public int[] GenerateMarks()
        {
            int[] marksArr = new int[University.amountOfMarks];
            for(int i = 0; i < University.amountOfMarks; i++)
            {
                //marksArr[i] = 2 + rnd.Next() % 4;

                // Поправка оценок
                int percent = 1 + rnd.Next() % 100;
                if (percent <= 5)
                    marksArr[i] = 2;
                else
                {
                    if (percent <= 30)
                        marksArr[i] = 3;
                    else
                    {
                        if (percent <= 80)
                            marksArr[i] = 4;
                        else
                            marksArr[i] = 5;
                    }
                }
            }

            return marksArr;
        }
    }
}

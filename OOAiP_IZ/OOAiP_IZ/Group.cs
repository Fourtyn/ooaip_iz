﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOAiP_IZ
{
    public class Group
    {
        public int Course;
        public string Name;

        public List<Student> students = new List<Student>();

        public Group(int course, string name)
        {
            Course = course;
            Name = name;
        }
    }
}

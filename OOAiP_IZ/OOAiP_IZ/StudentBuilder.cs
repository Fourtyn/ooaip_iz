﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOAiP_IZ
{
    public abstract class StudentBuilder
    {
        public abstract void SetStudent();
        public abstract void BuildName();
        public abstract void BuildGroup();
        public abstract void BuildCourse();
        public abstract void BuildMarks();
        public abstract void BuildStipend();
        public abstract void SetQualWork();
        public abstract Student GetResult();

    }

    public class Manager
    {
        StudentBuilder builder;
        public Manager(StudentBuilder builder)
        {
            this.builder = builder;
        }
        public void Construct()
        {
            builder.SetStudent();
            builder.BuildName();
            builder.BuildCourse();
            builder.BuildGroup();
            builder.BuildMarks();
            builder.SetQualWork();
            builder.BuildStipend();
        }
    }

    public class ConcreteStudentBuilder : StudentBuilder
    {
        Student product;
        Generator rand = Generator.Instance;

        public override void SetStudent()
        {
            product = new Student();
            product.id = University.Instance.Students.Count;
        }

        public override void BuildName()
        {
            product.name = rand.GenerateName();
        }
        public override void BuildCourse()
        {
            product.course = 1 + rand.rnd.Next() % 4;
        }
        public override void BuildGroup()
        {
            // Присваиваем студенту группу
            product.group = rand.GenerateGroup(product.course);
        }
        public override void BuildMarks()
        {
            product.marks = rand.GenerateMarks();
        }
        public override void BuildStipend()
        {
            if (rand.rnd.Next() % 2 == 1)
                product.isGettingStipend = true;
            else
                product.isGettingStipend = false;
        }
        public override void SetQualWork()
        {
            if (product.course == 4)
                product.work = new QualWork(product);
            else
                product.work = null;
        }
        public override Student GetResult()
        {
            return product;
        }
    }
}

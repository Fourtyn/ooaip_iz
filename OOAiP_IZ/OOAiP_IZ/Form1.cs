﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOAiP_IZ
{
    public partial class Form1 : Form
    {
        // Инициализация параметров формы - размера, возможности растягивать и стартового положения на экране
        private void InitFormParams()
        {
            this.StartPosition = FormStartPosition.CenterParent;
            this.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            this.Width = 1280;
            this.Height = 720;
            this.MaximizeBox = false;
        }
        public Form1()
        {
            InitializeComponent();

            InitFormParams();
        }

        private void buttonCreate_Click(object sender, EventArgs e)
        {
            University.Instance.CreateStudents((int)numericUpDownStudents.Value);

            Hide();
            Form2 mainForm = new Form2();
            mainForm.ShowDialog();
            Close();
        }
    }
}

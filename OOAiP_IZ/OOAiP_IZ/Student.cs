﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOAiP_IZ
{
    public class Student
    {
        public int id { get; set; }
        public string name { get; set; }
        public int course;

        public Group group;

        public bool isGettingStipend;
        public bool isToDismiss = false;
        public bool isToCourseUp = false;
        public bool isToGraduate = false;

        public int[] marks;

        public QualWork work = null;

        public int CountMarks(int[] marks, int markValue)
        {
            int amount = 0;
            for (int i = 0; i < marks.Length; i++)
            {
                if (marks[i] == markValue)
                    amount++;
            }

            return amount;
        }

        public string GetInfo()
        {
            StringBuilder sb = new StringBuilder();

            string[] names = name.Split(new char[] { ' ' });

            //sb.AppendLine("Информация о студенте:");
            
            sb.AppendLine("Фамилия: " + names[0]);
            sb.AppendLine("Имя: " + names[1]);
            sb.AppendLine("Отчество: " + names[2]);
            sb.AppendLine("Курс обучения: " + course);
            sb.AppendLine("Учебная группа: " + group.Name);

            sb.AppendLine("");
            sb.AppendLine("Кол-во оценок \"2\": " + CountMarks(marks, 2));
            sb.AppendLine("Кол-во оценок \"3\": " + CountMarks(marks, 3));
            sb.AppendLine("Кол-во оценок \"4\": " + CountMarks(marks, 4));
            sb.AppendLine("Кол-во оценок \"5\": " + CountMarks(marks, 5));
            sb.AppendLine("");

            if (course == 4 && work != null)
            {
                sb.AppendLine("Номер темы квал.работы: " + work.theme);
                sb.AppendLine("Научный руководитель: " + work.Teacher);
                sb.AppendLine("Оценка по квал.работе: " + work.mark);
                sb.AppendLine("");
            }

            sb.AppendLine("Состояние студента:");
            sb.AppendLine("Получает ли стипендию: " + (isGettingStipend ? "Да" : "Нет"));
            sb.AppendLine("Подлежит ли к отчислению: " + (isToDismiss ? "Да" : "Нет"));
            sb.AppendLine("Переведен ли на следующий курс: " + (isToCourseUp ? "Да" : "Нет"));
            sb.AppendLine("Подлежит ли к присвоению звания бакалавра: " + (isToGraduate ? "Да" : "Нет"));

            return sb.ToString();
        }
    }
}

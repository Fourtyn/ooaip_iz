﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOAiP_IZ
{
    public class University
    {
        // Одиночка
        public static readonly University _inst = new University();
        public static University Instance
        {
            get { return _inst; }
        }

        public University()
        {
            Students = new List<Student>();
            Groups = new List<Group>();
            Orders = new List<DeaneryOrder>();

            Deanery dean = new Deanery(new OrderCourseUpMaker(),
            new OrderDismissMaker(), new OrderGiveStipendMaker(),
            new OrderGraduateMaker(), new OrderQualWorksMaker());

            foreach (string gName in GroupNames)
            {
                Group gr = new Group((int)Char.GetNumericValue(gName[4]), gName);
                Groups.Add(gr);
            }

        }

        // Lists of data
        public string[] Teachers = { "Лутай Владимир Николаевич", "Проскуряков Александр Викторович",
            "Дроздов Сергей Николаевич", "Хусаинов Наиль Шавкятович", "Балабаева Ирина Юрьевна",
            "Скороход Сергей Васильевич", "Гуляев Никита Андреевич", "Данилова Анна Александровна",
            "Жиглатый Артемий Александрович", "Ковалева Карина Сергеевна", "Пилипушко Елена Михайловна",
            "Пилипушко Татьяна Александровна", "Селянкин Владимир Васильевич", "Родзина Ольга Николаевна"};

        public string[] GroupNames = { "КТбо1-1", "КТбо2-2", "КТбо2-3", "КТбо3-4", "КТбо4-5" };

        public List<Student> Students;
        public List<Group> Groups;

        public List<DeaneryOrder> Orders;

        // Constatns:
        public const int amountOfMarks = 5;

        // Methods
        public void CreateStudents(int amount)
        {
            StudentBuilder builder = new ConcreteStudentBuilder();
            Manager manager = new Manager(builder);
            for(int i = 0; i < amount; i++)
            {
                manager.Construct();
                Student st = builder.GetResult();
                Students.Add(st);

                // Добавляем студента в список студентов в группе
                Groups.Find(x => x.Name == st.group.Name).students.Add(st);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOAiP_IZ
{
    // Квалификационная работа
    public class QualWork
    {
        public string Teacher;
        public int mark;
        public int theme;
        public Student student;

        public QualWork(Student author)
        {
            Teacher = Generator.Instance.GenerateTeacher();
            mark = 2 + Generator.Instance.rnd.Next() % 4;
            theme = 1 + Generator.Instance.rnd.Next() % 100;
            student = author;
        }
    }
}